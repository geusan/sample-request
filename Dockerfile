FROM python3
FROM swig

ARG PATH="/program/pdfmanager"

COPY . ${PATH}

WORKDIR ${PATH}

RUN pip install -r requirements.txt

CMD ["bash"]